<?php

namespace Zabolots\CpaPromo;

use App\Offer;
use Illuminate\Console\Command;

class CpaPromoCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cpa:promo {feedId : Promo ID in promo_feeds table}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Download and process CPA feeds';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @throws \Exception
     */
    public function handle()
    {
        $this->info("Start");
        $feedId = $this->argument('feedId');

        /* @var $promoFeed PromoFeed */
        $promoFeed = PromoFeed::findOrFail($feedId);

        $cpa = $promoFeed->cpa;
        $cpaClass = CpaPromo::$cpaNets[$cpa];

        $cpaPromo = new CpaPromo(new $cpaClass(), $promoFeed);

        $xmlFileName = $promoFeed->id.date('_U').'.xml';
        $cpaPromo->store($promoFeed->url, $xmlFileName);

        $xmlPromoArray = $cpaPromo->getAllPromo($xmlFileName);

        $offerIdMap = Offer::whereNotNull('cpa_offer_id')
            ->get(['cpa_offer_id', 'id'])
            ->pluck('id', 'cpa_offer_id')
            ->toArray();

        $stat = $cpaPromo->storePromosToDB($xmlPromoArray, $offerIdMap);
        $cpaPromo->delete($xmlFileName);

        \Log::info("Promo feed $feedId was successfully processed.", $stat);
        $this->info("Promo feed $feedId was successfully processed. ({$stat['promoCnt']}/{$stat['toArchiveCnt']})");
    }
}
