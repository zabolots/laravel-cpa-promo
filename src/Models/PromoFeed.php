<?php

namespace Zabolots\CpaPromo;

use Illuminate\Database\Eloquent\Model;

/**
 * Zabolots\CpaPromo\PromoFeed
 *
 * @property int $id
 * @property string $cpa
 * @property string|null $name
 * @property string $url
 * @property int $enable
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Zabolots\CpaPromo\XmlPromo[] $XmlPromos
 * @method static \Illuminate\Database\Eloquent\Builder|\Zabolots\CpaPromo\PromoFeed whereCpa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Zabolots\CpaPromo\PromoFeed whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Zabolots\CpaPromo\PromoFeed whereEnable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Zabolots\CpaPromo\PromoFeed whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Zabolots\CpaPromo\PromoFeed whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Zabolots\CpaPromo\PromoFeed whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Zabolots\CpaPromo\PromoFeed whereUrl($value)
 * @mixin \Eloquent
 */
class PromoFeed extends Model
{
    public function XmlPromos()
    {
        return $this->hasMany(XmlPromo::class, 'promo_feed_id');
    }
}
