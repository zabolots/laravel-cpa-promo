<?php

namespace Zabolots\CpaPromo;

use Illuminate\Database\Eloquent\Model;

/**
 * Zabolots\CpaPromo\XmlPromo
 *
 * @property int $id
 * @property int $promo_feed_id
 * @property string $xml_promo_id
 * @property int $offer_id
 * @property string $title
 * @property int $type_id
 * @property string $description
 * @property string $code
 * @property string $url
 * @property string $date_start
 * @property string $date_end
 * @property int $archive
 * @property-read \Zabolots\CpaPromo\PromoFeed $promoFeed
 * @method static \Illuminate\Database\Eloquent\Builder|\Zabolots\CpaPromo\XmlPromo whereArchive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Zabolots\CpaPromo\XmlPromo whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Zabolots\CpaPromo\XmlPromo whereDateEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Zabolots\CpaPromo\XmlPromo whereDateStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Zabolots\CpaPromo\XmlPromo whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Zabolots\CpaPromo\XmlPromo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Zabolots\CpaPromo\XmlPromo whereOfferId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Zabolots\CpaPromo\XmlPromo wherePromoFeedId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Zabolots\CpaPromo\XmlPromo whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Zabolots\CpaPromo\XmlPromo whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Zabolots\CpaPromo\XmlPromo whereUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Zabolots\CpaPromo\XmlPromo whereXmlPromoId($value)
 * @mixin \Eloquent
 */
class XmlPromo extends Model
{
    public $timestamps = false;

    protected $dates = ['date_start', 'date_end'];

    public function promoFeed()
    {
        return $this->belongsTo(PromoFeed::class);
    }
}
