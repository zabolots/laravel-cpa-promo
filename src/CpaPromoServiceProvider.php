<?php

namespace Zabolots\CpaPromo;

use Illuminate\Support\ServiceProvider;

class CpaPromoServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');

        if ($this->app->runningInConsole()) {
            $this->commands([
                CpaPromoCommand::class,
            ]);
        }
    }
}