<?php

namespace Zabolots\CpaPromo;

use Carbon\Carbon;

interface CpaPromoInterface
{
    public function cpaName(): string;

    public function xpath(): string;

    public function offerId(\SimpleXMLElement $xmlPromo): int;

    public function promoId(\SimpleXMLElement $xmlPromo): string;

    public function promoTitle(\SimpleXMLElement $xmlPromo): string;

    public function promoDescription(\SimpleXMLElement $xmlPromo): string;

    public function promoUrl(\SimpleXMLElement $xmlPromo): string;

    public function promoCode(\SimpleXMLElement $xmlPromo): string;

    public function promoTypeId(\SimpleXMLElement $xmlPromo): int;

    public function promoDateStart(\SimpleXMLElement $xmlPromo): Carbon;

    public function promoDateEnd(\SimpleXMLElement $xmlPromo): Carbon;
}