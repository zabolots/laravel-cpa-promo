<?php

namespace Zabolots\CpaPromo;

use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;

class CpaPromo extends Controller
{
    const CpaPromoNetAdmitad = 'admitad';

    private $cpaDriver;

    private $promoFeed;

    public static $cpaNets = [
        self::CpaPromoNetAdmitad => CpaPromoAdmitadDriver::class,
    ];

    public $promoTypes = [
        1, // Бесплатная доставка
        2, // Деньги в подарок
        3, // Подарок к заказу
        4, // Скидка на заказ
        9, // Другое
    ];

    public function __construct(CpaPromoInterface $cpaDriver, PromoFeed $promoFeed)
    {
        $this->cpaDriver = $cpaDriver;
        $this->promoFeed = $promoFeed;
    }

    /**
     * Store content to file
     *
     * @param $url
     * @param $xmlFileName
     * @return bool
     */
    public function store($url, $xmlFileName)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $contents = curl_exec($ch);
        curl_close($ch);
        return \Storage::put($xmlFileName, $contents);
    }

    /**
     * Delete file
     *
     * @param $path
     * @return mixed
     */
    public function delete($xmlFileName)
    {
        return \Storage::delete($xmlFileName);
    }

    /**
     * Get all promo from xml and return it in array
     *
     * @param $xmlFileName
     * @return array
     * @throws \Exception
     */
    public function getAllPromo($xmlFileName): array
    {
        $reader = new \SimpleXMLReader();
        $reader->open(\Storage::path($xmlFileName));

        $xmlPromoArray = [];
        $promotions = function (\SimpleXMLReader $reader) use (&$xmlPromoArray) {
            try {
                $xmlPromo = $reader->expandSimpleXml();

                $xmlPromoId = $this->cpaDriver->promoId($xmlPromo);
                $xmlPromoArray[$xmlPromoId] = [
                    'offer_id' => trim($this->cpaDriver->offerId($xmlPromo)),
                    'title' => trim($this->cpaDriver->promoTitle($xmlPromo)),
                    'description' => trim($this->cpaDriver->promoDescription($xmlPromo)),
                    'url' => trim($this->cpaDriver->promoUrl($xmlPromo)),
                    'code' => trim($this->cpaDriver->promoCode($xmlPromo)),
                    'type_id' => trim($this->cpaDriver->promoTypeId($xmlPromo)),
                    'date_start' => $this->cpaDriver->promoDateStart($xmlPromo),
                    'date_end' => $this->cpaDriver->promoDateEnd($xmlPromo),
                ];
            }
            catch (\ErrorException $e)
            {
                \Log::error(
                    "Error while promo processing",
                    [
                        'Feed_id' => $this->promoFeed->id,
                        'msg' => $e->getMessage()
                    ]
                );
            }
            return true;
        };
        $reader->registerCallback($this->cpaDriver->xpath(), $promotions);
        $reader->parse();
        $reader->close();

        return $xmlPromoArray;
    }

    /**
     * Store promo from array to DB
     *
     * @param array $xmlPromoArray
     * @param array|null $offerIdMap
     * @return array
     */
    public function storePromosToDB(array $xmlPromoArray, array $offerIdMap = null)
    {
        $promoCnt = 0;
        foreach($xmlPromoArray as $xmlPromoId => $xmlPromo) {
            if ($offerIdMap and !isset($offerIdMap[$xmlPromo['offer_id']])) {
                continue;
            }

            // Map offer id
            $xmlPromo['offer_id'] = $offerIdMap ?
                $offerIdMap[$xmlPromo['offer_id']] :
                $xmlPromo['offer_id'];

            $xmlPromo['archive'] = false;

            try
            {
                $updResult = XmlPromo::updateOrInsert(
                    [
                        'promo_feed_id' => $this->promoFeed->id,
                        'xml_promo_id' => $xmlPromoId
                    ],
                    $xmlPromo
                );
                if ($updResult) {
                    $promoCnt++;
                }
            }
            catch (QueryException $e)
            {
                \Log::error("Insert promo to DB failed",
                    [
                        'Feed_id' => $this->promoFeed->id,
                        'xml_promo_id' => $xmlPromoId,
                        'msg' => $e->getMessage()
                    ]);
            }
        }

        // Archive all promo if they are not present in feed
        $toArchiveCnt = XmlPromo::wherePromoFeedId($this->promoFeed->id)
            ->where('archive', false)
            ->whereNotIn('xml_promo_id', array_keys($xmlPromoArray))
            ->update(['archive' => true]);

        return [
            'promoCnt' => $promoCnt,
            'toArchiveCnt' => $toArchiveCnt
        ];
    }
}