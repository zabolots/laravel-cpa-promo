<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateXmlPromosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xml_promos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedMediumInteger('promo_feed_id')
                ->index();
            $table->string('xml_promo_id', 20)
                ->index();
            $table->unsignedSmallInteger('offer_id');
            $table->string('title', 180);
            $table->unsignedSmallInteger('type_id');
            $table->text('description');
            $table->string('code', 32);
            $table->string('url');
            $table->date('date_start')
                ->index();
            $table->date('date_end')
                ->index();
            $table->boolean('archive')
                ->default(false)
                ->index();

            $table->unique([
                'promo_feed_id',
                'xml_promo_id'
            ]);
            $table->foreign('promo_feed_id')
                ->references('id')
                ->on('promo_feeds')
                ->onUpdate('cascade')
                ->onDelete('restrict');

            if (Schema::hasTable('offers')) {
                $table->foreign('offer_id')
                    ->references('id')
                    ->on('offers')
                    ->onUpdate('cascade')
                    ->onDelete('restrict');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xml_promos');
    }
}
