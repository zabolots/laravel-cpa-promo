<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromoFeedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promo_feeds', function (Blueprint $table) {
            $table->mediumIncrements('id');
            $table->string('cpa', 64)->index();
            $table->string('name')->nullable();
            $table->string('url');
            $table->boolean('enable')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promo_feeds');
    }
}
