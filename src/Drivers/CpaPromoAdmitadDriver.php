<?php

namespace Zabolots\CpaPromo;


use Carbon\Carbon;

class CpaPromoAdmitadDriver implements CpaPromoInterface
{
    private $promoTypesMap = [
        1 => 1,
        2 => 4,
        3 => 3,
        4 => 2,
        9 => 5,
    ];

    private $promoTypeUnknownId = 9;

    public function cpaName(): string
    {
        return CpaPromo::CpaPromoNetAdmitad;
    }

    public function xpath(): string
    {
        return '/admitad_coupons/coupons/coupon';
    }

    public function offerId(\SimpleXMLElement $xmlPromo): int
    {
        return (int)$xmlPromo->advcampaign_id;
    }

    public function promoId(\SimpleXMLElement$xmlPromo): string
    {
        return (string)$xmlPromo['id'];
    }

    public function promoTitle(\SimpleXMLElement $xmlPromo): string
    {
        return (string)$xmlPromo->name;
    }

    public function promoDescription(\SimpleXMLElement $xmlPromo): string
    {
        return (string)$xmlPromo->description;
    }

    public function promoUrl(\SimpleXMLElement $xmlPromo): string
    {
        return (string)$xmlPromo->gotolink;
    }

    public function promoCode(\SimpleXMLElement $xmlPromo): string
    {
        $promoCode = (string)$xmlPromo->promocode;
        if ($promoCode == 'Не нужен') {
            $promoCode = '';
        }
        return $promoCode;
    }

    public function promoTypeId(\SimpleXMLElement $xmlPromo): int
    {
        $xmlPromoTypeId = (int)$xmlPromo->types->type_id;
        return $this->promoTypesMap[$xmlPromoTypeId] ?: $this->promoTypeUnknownId;
    }

    public function promoDateStart(\SimpleXMLElement $xmlPromo): Carbon
    {
        return Carbon::parse($xmlPromo->date_start);
    }

    public function promoDateEnd(\SimpleXMLElement $xmlPromo): Carbon
    {
        $promoDateEnd = Carbon::parse($xmlPromo->date_end);
        if ($promoDateEnd == 'None') {
            $promoDateEnd = Carbon::parse($xmlPromo->date_start)->addYear(2);
        }
        return $promoDateEnd;
    }
}